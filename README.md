# README #

### What is this repository for? ###

Application for pulling and storing macroeconomic data using the Federal Reserve's FRED API.

### How do I get set up? ###

* Create jar using the command:
sbt assembly

The previous command builds the application, run the tests and creates the jar for its execution.

To install sbt please use the following link:
http://www.scala-sbt.org/1.0/docs/Getting-Started.html

* Execute the application using the following command:

java -jar fred-app.jar

The program do not use arguments for its execution

### Assumptions and Comments ###
- When the value field of an observation does not contain a number, the value assigned for that observation is 0.
For example, in the case of the UMCSENT series, the value field in some observations contains "." instead of a number.

- The SQL script to create the schema is provided, however the schema is created with the Scala application using the Slick library.
