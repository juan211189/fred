package com.fc.fred

import java.sql.Date
import java.text.SimpleDateFormat
import spray.json.{DefaultJsonProtocol, DeserializationException, JsObject, JsString, JsValue, RootJsonFormat}
import scala.util.Try
/**
  * Created by juan211189 on 22.08.17.
  * Customized Json format
  */
object FredJsonProtocol extends DefaultJsonProtocol {
  implicit object FredObsFormat extends RootJsonFormat[FredObservation] {
    def write(fredObs: FredObservation) = JsObject(
      "realtime_start" -> JsString(fredObs.realTimeStart.toString),
      "realtime_end" -> JsString(fredObs.realTimeEnd.toString),
      "date" -> JsString(fredObs.valueDate.toString),
      "value" -> JsString(fredObs.value.toString)
    )
    def read(value: JsValue) = {
      val format = new SimpleDateFormat("yyyy-MM-dd")
      value.asJsObject.getFields("realtime_start", "realtime_end", "date", "value") match {
        case Seq(JsString(realTimeStart), JsString(realTimeEnd), JsString(valueDate), JsString(value)) =>
          new FredObservation(new Date(format.parse(realTimeStart).getTime), new Date(format.parse(realTimeEnd).getTime),
            new Date(format.parse(valueDate).getTime), if (Try(value.toDouble).isSuccess) value.toDouble else 0.0)
        case _ => throw new DeserializationException("com.fc.fred.FredObservation expected")
      }
    }
  }
}