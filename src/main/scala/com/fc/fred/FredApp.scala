package com.fc.fred

import spray.json._
import FredJsonProtocol._
/**
  * Created by juan211189 on 22.08.17.
  */
object FredApp {

  val EndpointUNRATE: String = "https://api.stlouisfed.org/fred/series/observations?series_id=UNRATE&" +
    "api_key=71d26b4a69c692f64dcb9a4ccb5f738b&file_type=json"
  val EndpointGDPC: String = "https://api.stlouisfed.org/fred/series/observations?series_id=GDPC1&" +
    "api_key=71d26b4a69c692f64dcb9a4ccb5f738b&file_type=json"
  val EndpointUMCSENT: String = "https://api.stlouisfed.org/fred/series/observations?series_id=UMCSENT&" +
    "api_key=71d26b4a69c692f64dcb9a4ccb5f738b&file_type=json"

  def main(args: Array[String]): Unit = {
    FredObservationDAO.dropSchema()
    FredObservationDAO.createSchema()
    println("Schema created.")
    processSeries(EndpointGDPC, "GDPC1")
    println("GDPC1 series processed.")
    processSeries(EndpointUMCSENT, "UMCSENT")
    println("UMCSENT series processed.")
    processSeries(EndpointUNRATE, "UNRATE")
    println("UNRATE series processed.")
    FredObservationDAO.close()
    println("Process finished.")
  }

  /**
    * Processes a series of observations (Gets them from a REST URL and stores them in a database)
    */
  def processSeries(endpoint: String, seriesType: String): Unit = {
    try {
      val content = getResponse(endpoint)
      val fredObsList = convertObservations(content, seriesType)
      FredObservationDAO.insertAll(fredObsList)
    } catch {
      case ioe: java.io.IOException => println(ioe.getMessage)
      case ste: java.net.SocketTimeoutException => println("The remote application is not responding\n"+println(ste))
    }
  }

  /**
    * Converts the observations contained in a String to a List of FredObservation objects
    */
  def convertObservations(content: String, seriesType: String): List[FredObservation] = {
    val obsArray = content.parseJson.asJsObject.getFields("observations").head.asInstanceOf[JsArray]
    obsArray.elements.map{obs =>
      val fredObservation = obs.convertTo[FredObservation]
      fredObservation.seriesType = Some(seriesType)
      fredObservation
    }.toList
  }

  /**
    * Returns the text (content) from a REST URL as a String.
    */
  @throws(classOf[java.io.IOException])
  @throws(classOf[java.net.SocketTimeoutException])
  def getResponse(url: String,
                  connectTimeout: Int = 5000,
                  readTimeout: Int = 5000,
                  requestMethod: String = "GET") =
  {
    import java.net.{HttpURLConnection, URL}
    val connection = new URL(url).openConnection.asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(connectTimeout)
    connection.setReadTimeout(readTimeout)
    connection.setRequestMethod(requestMethod)
    val inputStream = connection.getInputStream
    val content = io.Source.fromInputStream(inputStream).mkString
    if (inputStream != null) inputStream.close
    content
  }
}