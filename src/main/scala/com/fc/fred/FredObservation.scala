package com.fc.fred

import java.sql.Date

import slick.driver.PostgresDriver.api._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

/**
  * Created by juan211189 on 22.08.17.
  */
case class FredObservation(realTimeStart: Date, realTimeEnd: Date, valueDate: Date, value: Double,
                           var seriesType: Option[String] = None, id: Option[Int] = None)

class FredObservations(tag: Tag) extends Table[FredObservation](tag, "FRED_OBSERVATIONS") {
  // Auto Increment the id primary key column
  def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
  def realTimeStart = column[Date]("REAL_TIME_START", O.NotNull)
  def realTimeEnd = column[Date]("REAL_TIME_END", O.NotNull)
  def valueDate = column[Date]("DATE", O.NotNull)
  def value = column[Double]("VALUE", O.NotNull)
  def seriesType = column[String]("SERIES_TYPE", O.NotNull)
  def * = (realTimeStart, realTimeEnd, valueDate, value, seriesType.?, id.?) <> (FredObservation.tupled, FredObservation.unapply)
}

object FredObservationDAO {
  val db: Database = Database.forConfig("postgresDB")
  val fredObservations: TableQuery[FredObservations] = TableQuery[FredObservations]

  def dropSchema(): Unit = db.run(fredObservations.schema.drop)
  def createSchema(): Unit = Await.result(db.run(DBIO.seq(fredObservations.schema.create)), Duration.Inf)
  def insertAll(fredObsList: List[FredObservation]): Unit =
    Await.result(db.run(DBIO.seq(fredObservations ++= fredObsList)), Duration.Inf)
  def close(): Unit = db.close()
}