import java.sql.Date
import java.util.Calendar

import com.fc.fred.{FredObservation, FredObservations}
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Seconds, Span}
import slick.driver.PostgresDriver.api._
import slick.jdbc.meta._

/**
  * Created by juan211189 on 24.08.17.
  */
class TablesSuite extends FunSuite with BeforeAndAfter with ScalaFutures {
  implicit override val patienceConfig = PatienceConfig(timeout = Span(5, Seconds))

  val fredObservations = TableQuery[FredObservations]

  var db: Database = _

  def createSchema() =
    db.run(fredObservations.schema.create).futureValue

  val currentDate = new Date(Calendar.getInstance().getTimeInMillis)
  def insertFredObservation(): Int =
    db.run(fredObservations += FredObservation(currentDate, currentDate, currentDate, 5.5,
      Some("testType"))).futureValue


  before { db = Database.forConfig("h2mem1") }

  test("Creating the Schema works") {
    createSchema()

    val tables = db.run(MTable.getTables).futureValue
    val exists = tables.count(_.name.name.equalsIgnoreCase("FRED_OBSERVATIONS")) == 1
    assert(exists)
  }

  test("Inserting a FredObservation works") {
    createSchema()

    val insertCount = insertFredObservation()
    assert(insertCount == 1)
  }

  test("Query FredObservations works") {
    createSchema()
    insertFredObservation()
    val results = db.run(fredObservations.result).futureValue
    assert(results.size == 1)
    assert(results.head.id.get == 1)
  }

  after { db.close }
}
