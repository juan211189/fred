import java.sql.Date
import com.fc.fred.FredJsonProtocol._
import com.fc.fred.FredObservation
import org.scalatest.FlatSpec
import spray.json._
/**
  * Created by juan211189 on 24.08.17.
  */
class FredJsonSuite extends FlatSpec{

  val date = new Date(117, 7, 22)
  val fredObsJsonString =
    """{"realtime_start":"2017-08-22","realtime_end":"2017-08-22","date":"2017-08-22","value":"0.0"}"""

  val fredObservation = FredObservation(date, date, date, 0.0)

  "A fred observation" should "convert from json string format to FredObservation object" in {

    val fredObsFromJson = fredObsJsonString.parseJson.convertTo[FredObservation]

    assert(fredObsFromJson.realTimeStart === fredObservation.realTimeStart &&
      fredObsFromJson.realTimeEnd === fredObservation.realTimeEnd &&
      fredObsFromJson.valueDate === fredObservation.valueDate &&
      fredObsFromJson.value === fredObservation.value)
  }

  it should "convert from FredObservation object to json string format" in {
    assert(fredObservation.toJson.compactPrint === fredObsJsonString)
  }
}