name := "fred-app"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies ++= List(
  "io.spray" %%  "spray-json" % "1.3.3",
  "com.typesafe.slick" %% "slick" % "3.0.0",
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.h2database" % "h2" % "1.3.175",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  //"com.zaxxer" % "HikariCP-java6" % "2.3.2",
  "com.zaxxer" % "HikariCP" % "2.4.1",
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
)

    